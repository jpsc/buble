import ArrayExpression from './ArrayExpression.js';
import ArrowFunctionExpression from './ArrowFunctionExpression.js';
import AssignmentExpression from './AssignmentExpression.js';
import CallExpression from './CallExpression.js';
import ClassBody from './ClassBody.js';
import ClassDeclaration from './ClassDeclaration.js';
import ClassExpression from './ClassExpression.js';
import ExportDefaultDeclaration from './ExportDefaultDeclaration.js';
import ForStatement from './ForStatement.js';
import ForOfStatement from './ForOfStatement.js';
import FunctionDeclaration from './FunctionDeclaration.js';
import FunctionExpression from './FunctionExpression.js';
import ImportSpecifier from './ImportSpecifier.js';
import ImportDefaultSpecifier from './ImportDefaultSpecifier.js';
import Literal from './Literal.js';
import Identifier from './Identifier.js';
import Property from './Property.js';
import Super from './Super.js';
import TaggedTemplateExpression from './TaggedTemplateExpression.js';
import TemplateElement from './TemplateElement.js';
import TemplateLiteral from './TemplateLiteral.js';
import ThisExpression from './ThisExpression.js';
import UpdateExpression from './UpdateExpression.js';
import VariableDeclaration from './VariableDeclaration.js';
import VariableDeclarator from './VariableDeclarator.js';

export default {
	ArrayExpression,
	ArrowFunctionExpression,
	AssignmentExpression,
	CallExpression,
	ClassBody,
	ClassDeclaration,
	ClassExpression,
	ExportDefaultDeclaration,
	ForStatement,
	ForOfStatement,
	FunctionDeclaration,
	FunctionExpression,
	ImportSpecifier,
	ImportDefaultSpecifier,
	Literal,
	Identifier,
	Property,
	Super,
	TaggedTemplateExpression,
	TemplateElement,
	TemplateLiteral,
	ThisExpression,
	UpdateExpression,
	VariableDeclaration,
	VariableDeclarator
};
